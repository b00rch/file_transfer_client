
const fs      = require('fs');
const path    = require('path');
const axios   = require('axios');
const https   = require('https');
const colors  = require('colors');
const mkdirp  = require('mkdirp');
const express = require('express');
const config  = require('../config');
const redis   = require('../helpers/redis');
const stats   = require('../helpers/stats');
const debug   = require('debug')('able:data');

const { get_str_size }   = require('../helpers/file_size_converter');

const router = express.Router();
const server_host = (config.ssl ? 'https' : 'http') +'://'+ config.server.host + ':'+ config.server.port;
const HUNDRED_MB = 100 * 1024 * 1024;

/* GET users listing. */
router.post('/', async function(req, res) {
	console.log('\n');
    const checksum  = req.body.checksum;
    const file_size = req.body.size;
	const file_path = req.body.path;
	debug(config.path, file_path);

	// protect local file inclusion
	const full_path = path.resolve(config.path, file_path);
	if(full_path.search(config.path) === -1) {
		res.json({
			error : true,
			msg   : "Local file inclusion attack"
		});
		return;
	}

	let file_dir = file_path.split(path.sep);
	const file_name = file_dir.pop();
	file_dir = path.resolve(config.path, ...file_dir);
	debug(file_dir);

	try {
		if(!fs.existsSync(file_dir)) {
			console.log('create directory: '+ file_dir.cyan);
			mkdirp.sync(file_dir);
		}
	}
	catch(err)  {
		console.error(err);
		// res.status(500).send(err.message);
		res.json({
			error : true,
			msg   : err.message
		});
		return;
	}

	// file_path = 'YWJZsXNvZnQ=/name_card/123.jpg';
	debug('checksum: '+ checksum +' [size:'+ file_size+'] '+ file_path);



	// if file exists and size is same
	if(fs.existsSync(full_path)) {
		const file_stat = fs.statSync(full_path);
		if(file_stat.size === file_size) {
			console.log('already copied. '+ file_path.cyan);
			// save to redis
			await redis.hset('file-'+ checksum, file_path, JSON.stringify({ size: file_size, date: Date.now() }));
			res.json({res: 'already'});
			return;
		}
	}

	try {
		const paths = await redis.hkeys('file-'+ checksum);
		// if hard linked before
		if(paths.length)  {
			debug(paths);
			for (var i = paths.length - 1; i >= 0; i--) {
				const path_in_redis = path.resolve(config.path, paths[i]);

				if(fs.existsSync(path_in_redis))  {
					if(path_in_redis !== full_path)  {
						fs.linkSync(path_in_redis, full_path);
						// save to redis
						await redis.hset('file-'+ checksum, file_path, JSON.stringify({ size: file_size, date: Date.now() }));
						console.log('create hard link '+ file_path.cyan);
						res.json({res: 'hard_linked'});
						return;
					}
				}
				else  {
					await redis.hdel('file-'+ checksum, paths[i]);
				}	
			}
		}


		// download file
		console.log('downloading file '+ file_path.cyan);
		const start_time = Date.now();
		const result = await axios({
			url: server_host +'/data/'+ encodeURIComponent(file_path),
			method: 'GET',
			// responseType: 'text/plain',
			httpsAgent: new https.Agent({ rejectUnauthorized: false }),
			responseType: 'stream'
		});
		// console.log(result.headers['content-length']);
		const size = parseInt(result.headers['content-length']);
		const millis = Date.now() - start_time;

		console.log(('downloaded size: '+ size +'\tfile_size: '+ file_size).green);

		// change statistics
		// console.log(typeof stats.tranfered_data);
		stats.elapsed_time += millis;
		stats.tranfered_data += size;
		stats.tmp_elapsed_time += millis;
		stats.tmp_tranfered_data += size;
		if(stats.tmp_tranfered_data > HUNDRED_MB)  {
			const elapsed_seconds = stats.tmp_elapsed_time / 1000; // seconds
			let data_in_a_second = stats.tmp_tranfered_data / elapsed_seconds;
			data_in_a_second = Math.round(data_in_a_second / 1024 / 1024 * 100) / 100;
			stats.download_speed = data_in_a_second +'MB/s';
			stats.tmp_elapsed_time = 0;
			stats.tmp_tranfered_data = 0;
			console.log(('speed: '+ stats.download_speed +', total downloaded: '+get_str_size(stats.tranfered_data)).green);
		}
		else {
			console.log(('total downloaded data:'+ get_str_size(stats.tranfered_data) +' and seconds:'+ (stats.elapsed_time / 1000)).green);
		}

		// write file to filesystem
		const writer = fs.createWriteStream(full_path);
		result.data.pipe(writer);

		writer.on('finish', ()=>console.log('downloaded file '+ file_path.cyan));
		writer.on('error', console.log);

		// save to redis
		await redis.hset('file-'+ checksum, file_path, JSON.stringify({ size, date: Date.now() }));
		res.json({res: 'done'});
	}
	catch(err) {
		console.error(err);
		// res.status(500).send(err.message);
		res.json({
			error : true,
			msg   : err.message
		});
	}
});

module.exports = router;
