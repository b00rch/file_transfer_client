/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : redis.js
* Created at  : 2020-05-26
* Updated at  : 2020-05-26
* Author      : Boorch
* Purpose     : 
* Description : 
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";

var debug = require('debug')('able:redis');
var promiseFactory = require('q').Promise;
var redis = require('promise-redis')(promiseFactory);

const config = require('../config');

var client = redis.createClient(config.redis);
client.on("connect", function () {
	console.log("REDIS CONNECT SUCCESSFUL. "+ (new Date).toString());
});
client.on("error", function (err) {
    console.log("REDIS CONNECTION ERROR: " + err +". "+ (new Date).toString());
});
client.on("end", function () {
    console.log("REDIS CONNECTION CLOSED."+ (new Date).toString());
});

module.exports = client