/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : fs.js
* Created at  : 2019-10-22
* Updated at  : 2019-10-22
* Author      : Boorch
* Purpose     : 
* Description : 
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";

const Fs = require('fs');
Fs.is_dir = (path) => {
	return Fs.lstatSync(path).isDirectory();
};
Fs.is_file = (path) => {
	return Fs.lstatSync(path).isFile();
};
Fs.is_symbolic_link = (path) => {
	return Fs.lstatSync(path).isSymbolicLink();
};
module.exports = Fs;