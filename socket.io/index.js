/* -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.
* File Name   : index.js
* Created at  : 2019-07-18
* Updated at  : 2019-07-18
* Author      : Boorch
* Purpose     : 
* Description : 
_._._._._._._._._._._._._._._._._._._._._.*/

"use strict";

const debug = require('debug')('able:socket.io');
// const disk = require('diskusage');
const IO = require('socket.io-client');
const config = require('../config');

const { get_str_size } = require('../helpers/file_size_converter');
const stats = require('../helpers/stats');

let free_size=0;
// try {
// 	let info = disk.checkSync(config.path);
// 	free_size = info.available - 512 * 1024 * 1024;
// 	console.log('free disk space: '+ get_str_size(free_size));
// }
// catch (err) {
// 	console.log(err);
// 	process.exit();
// }

module.exports = async server => {

  	const _server = server.address();
	const host = (config.ssl ? 'https' : 'http') +'://'+ config.addr +':'+ config.port;
  	debug(host);
	
	let is_connected = false;

	const socket = IO((config.ssl ? 'https' : 'http') +'://'+ config.server.host + ':'+ config.server.port, {
		query: {
			data_dir: config.data_dir || '',
			name: config.name,
			free_size,
			host
		}
	});
	socket.on('connect', () => {
		console.log('Successfuly connected to Storage Server.');
		is_connected = true;
	});
	socket.on('error', (error) => {
		console.error(error);
		is_connected = false;
	});
	socket.on('disconnect', () => {
		is_connected = false;
		console.log(('total donwloaded'+
			'\ndata:'+ get_str_size(stats.tranfered_data) +
			'\nseconds:'+ (stats.elapsed_time / 1000) +
			'\nspeed:'+ stats.download_speed).green);
		stats.elapsed_time = 0;
		stats.tranfered_data = 0;
		stats.tmp_elapsed_time = 0;
		stats.tmp_tranfered_data = 0;
		stats.download_speed = '';
		console.log('Disconnected from Storage Server');
		// process.exit();
	});

};